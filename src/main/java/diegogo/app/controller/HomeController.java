package diegogo.app.controller;

import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

@Controller
public class HomeController {
	
	@RequestMapping(value="/inicio", method=RequestMethod.GET)
	public String inicio(Model model) {
		//Model - Permite pasar atributos a la vista
		model.addAttribute("nombre", "Diego Alvirde Martinez");
		return "inicio";
	}
	
}
